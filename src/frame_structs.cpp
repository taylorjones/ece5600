#include "frame_structs.h"

//Default checksum is 20 bytes for header
uint16_t network_helper::checksum(octet* frame) {
  checksum(frame,20);
}

//Generate checksum for N bytes
uint16_t network_helper::checksum(octet* frame, int len) {
  uint32_t result = 0;
  for(int i=0;i<len;i++) {
    if(i%2==0) {
      result = result + (((uint32_t)frame[i]) << 8);
    } else {
      result = result + ((uint32_t)frame[i]);
    }
  }
  result = (result >> 16) + (result & 0xFFFF);
  return ~((uint16_t) result);
}

//Process Incoming IP and filter checksum and destination
//Send ICMP to process ICMP
void network_helper::processIP(ip_frame frame) {
  uint16_t check = checksum((octet*)&frame);
  if(0!=check) {
    return;
  } else {
  }
  if(0!=std::memcmp(frame.dst_ip,myIp,4)) {
    return;
  }
  if(frame.prot[0]==1) {
    processICMP(frame.src_ip, frame.total_len, frame.data.as_icmp);
  } else {
    // std::cout << "IP Received: ";
    // print_ip(frame.src_ip);
    // std::cout << "->";
    // print_ip(frame.dst_ip);
    // std::cout << std::endl;
  }
}

//Process Incoming ICMP and send reply if it is a request
void network_helper::processICMP(octet* ip, octet* length, icmp_frame frame) {
  std::cout << "\nICMP Received From ";
  print_ip(ip);
  std::cout << std::endl;
  print_bytes(&frame, 4);
  print_bytes(&frame, 4,4);
  if(frame.type[0] == 0x08) {
    ip_frame ipframe;
    std::memcpy(ipframe.src_ip, myIp, 4);
    std::memcpy(ipframe.dst_ip, ip, 4);
    ipframe.hdr_len[0] = 0x45;
    ipframe.dscp[0] = 0x0;
    ipframe.total_len[0] = length[0];
    ipframe.total_len[1] = length[1];
    ipframe.prot[0] = 0x1;
    ipframe.data.as_icmp = frame;
    ipframe.data.as_icmp.type[0] = 0x00;
    ipframe.data.as_icmp.checksum[0] = 0x00;
    ipframe.data.as_icmp.checksum[1] = 0x00;
    uint16_t check = checksum((octet*)&ipframe.data.as_icmp,(length[0]*256+length[1]-20));
    ipframe.data.as_icmp.checksum[0] = (check>>8)&0xFF;
    ipframe.data.as_icmp.checksum[1] = (check)&0xFF;
    sendIPFrame(ipframe);
  }
}

//Process incoming ARP packets. Put MAC in cache
void network_helper::processArp(ether_frame frame) {
  //Put the ARP in the cache
  putArpCache(frame.data.as_arp.src_ip, frame.data.as_arp.src_mac);
  //If the frame is a request and matches our IP, make a new reply frame.
  if(frame.data.as_arp.opcode[1]==1 && compare_ip(frame.data.as_arp.dst_ip, myIp)) {
    //sendARP(frame.data.as_arp.src_ip,1);
  }
}

//Check if IP is in cache
bool network_helper::isIPInCache(octet * ip) {
  uint32_t mIp = ip[3] | ip[2] << 8 | ip[1] << 16 | ip[0] << 24;
  auto iter = arp_cache.find(mIp);
  return iter!=arp_cache.end();
}

//Get MAC if IP exists
octet * network_helper::getMACFromCache(octet * ip) {
  uint32_t mIp = ip[3] | ip[2] << 8 | ip[1] << 16 | ip[0] << 24;
  auto iter = arp_cache.find(mIp);
  return iter->second;
}

//Put an ARP in the cache if it isn't already
void network_helper::putArpCache(octet * ip, octet * mac) {
  uint32_t mIp = ip[3] | ip[2] << 8 | ip[1] << 16 | ip[0] << 24;
  octet* sMac = new octet[6];
  std::memcpy(sMac, mac, 6);
  auto result = arp_cache.insert( std::pair<uint32_t,octet *>(mIp, sMac) );
  if(result.second) {
    checkQueue(ip);
  } else {
    delete sMac;
  }
}

//Send an ARP message if the IP is already in the cache, otherwise do a
//broadcast and wait for a response with a MAC address
void network_helper::sendARP(octet* ip, bool reply) {
  std::cout << "\nSending ARP to: ";
  print_ip(ip);
  std::cout << std::endl;
  ether_frame frame;
  std::memcpy(frame.src_mac, myMac, 6);
  std::memcpy(frame.data.as_arp.src_mac, myMac, 6);
  std::memcpy(frame.data.as_arp.src_ip, myIp, 4);
  std::memcpy(frame.data.as_arp.dst_ip, ip, 4);
  frame.prot[0] = 0x08;
  frame.prot[1] = 0x06;
  frame.data.as_arp.hw[0] = 0x00;
  frame.data.as_arp.hw[1] = 0x01;
  frame.data.as_arp.prot[0] = 0x08;
  frame.data.as_arp.prot[1] = 0x00;
  frame.data.as_arp.hw_size[0] = 0x06;
  frame.data.as_arp.prot_size[0] = 0x04;
  if(isIPInCache(ip)) {
    auto mac = getMACFromCache(ip);
    std::memcpy(frame.data.as_arp.dst_mac, mac, 6);
    frame.data.as_arp.opcode[0] = 0x00;
    frame.data.as_arp.opcode[1] = reply?0x02:0x01;
  } else {
    frame.data.as_arp.dst_mac[0] = 0xFF;
    frame.data.as_arp.dst_mac[1] = 0xFF;
    frame.data.as_arp.dst_mac[2] = 0xFF;
    frame.data.as_arp.dst_mac[3] = 0xFF;
    frame.data.as_arp.dst_mac[4] = 0xFF;
    frame.data.as_arp.dst_mac[5] = 0xFF;
    frame.data.as_arp.opcode[0] = 0x00;
    frame.data.as_arp.opcode[1] = 0x01;
    addArpToQueue(ip, reply);
  }
  for(int i = 0; i < 18; i++)
    frame.data.as_arp.data[i] = 0x00; //make the data portion all zeroes
  std::memcpy(frame.dst_mac, frame.data.as_arp.dst_mac,6);
  print_bytes(&frame, 42);
  net->send_frame(&frame, 42);
}

//Get IP of next hop based on subnet mask
void network_helper::getRouteIP(octet *ip, octet *result) {
  if(
    ((ip[0]&((netMask>>24)&0xff)) == (myIp[0]&((netMask >> 24)&0xff))) &&
    ((ip[1]&((netMask>>16)&0xff)) == (myIp[1]&((netMask >> 16)&0xff))) &&
    ((ip[2]&((netMask>>8 )&0xff)) == (myIp[2]&((netMask >>  8)&0xff))) &&
    ((ip[3]&((netMask    )&0xff)) == (myIp[3]&((netMask      )&0xff)))
  ) {
    std::memcpy(result, ip, 4);
  } else {
    std::memcpy(result, gateway, 4);
  }
}

//Send ICMP request
void network_helper::sendICMP(octet* ip) {
  seq++;
  ip_frame frame;
  std::memcpy(frame.src_ip, myIp, 4);
  std::memcpy(frame.dst_ip, ip, 4);
  frame.hdr_len[0] = 0x45;
  frame.dscp[0] = 0x0;
  frame.total_len[0] = 0x00;
  frame.total_len[1] = 0x54;
  frame.prot[0] = 0x1;
  frame.data.as_icmp.type[0] = 0x08;
  frame.data.as_icmp.code[0] = 0x00;
  frame.data.as_icmp.checksum[0] = 0;
  frame.data.as_icmp.checksum[1] = 0;
  frame.data.as_icmp.add_header[0] = 0x42;
  frame.data.as_icmp.add_header[1] = 0xbb;
  frame.data.as_icmp.add_header[2] = 0xFF&(seq>>8);
  frame.data.as_icmp.add_header[3] = 0xFF&seq;
  uint16_t check = checksum((octet*)&frame.data.as_icmp,64);
  frame.data.as_icmp.checksum[0] = (check>>8)&0xFF;
  frame.data.as_icmp.checksum[1] = (check)&0xFF;
  sendIPFrame(frame);
}

//Send IP Frame
//Calculate checksum
void network_helper::sendIPFrame(ip_frame ipframe) {
  std::cout << "\nSending IP to: ";
  print_ip(ipframe.dst_ip);
  std::cout << std::endl;
  ether_frame frame;
  std::memcpy(frame.src_mac, myMac, 6);
  frame.prot[0] = 0x08;
  frame.prot[1] = 0x00;
  octet route_ip[4];
  getRouteIP(ipframe.dst_ip, route_ip);
  if(isIPInCache(route_ip)) {
    auto mac = getMACFromCache(route_ip);
    std::memcpy(frame.dst_mac, mac, 6);
    ++ident;
    ipframe.ident[0] = 0xFF&(ident>>8);
    ipframe.ident[1] = 0xFF&ident;
    ipframe.flags = 0;
    ipframe.frag_ofst = 0;
    ipframe.ttl[0] = 0x40;
    ipframe.hdr_chk[0] = 0;
    ipframe.hdr_chk[1] = 0;
    uint16_t check = checksum((octet*)&ipframe);
    ipframe.hdr_chk[0] = (check>>8)&0xFF;
    ipframe.hdr_chk[1] = (check)&0xFF;
    frame.data.as_ip = ipframe;
    net->send_frame(&frame, ipframe.total_len[0]*0x100+ipframe.total_len[1]+14);
  } else {
    sendARP(route_ip, false);
    outgoing_ipframe_queue.push_back(ipframe);
  }
}

//Add an ARP to outgoing queue
void network_helper::addArpToQueue(octet* ip, bool reply) {
  queued_message msg;
  std::memcpy(msg.ip, ip, 4);
  msg.reply = reply;
  outgoing_message_queue.push_back(msg);
}

//Remove messages after they are sent
void network_helper::checkQueue(octet* ip) {
  int i = 0;
  while(i < outgoing_message_queue.size()) {
    if(std::memcmp(ip, outgoing_message_queue[i].ip,4)==0) {
      sendARP(outgoing_message_queue[i].ip, outgoing_message_queue[i].reply);
      outgoing_message_queue.erase(outgoing_message_queue.begin() + i);
    } else {
      ++i;
    }
  }
  i = 0;
  octet route_ip[4];
  while(i < outgoing_ipframe_queue.size()) {
    getRouteIP(outgoing_ipframe_queue[i].dst_ip, route_ip);
    if(std::memcmp(ip, route_ip,4)==0) {
      sendIPFrame(outgoing_ipframe_queue[i]);
      outgoing_ipframe_queue.erase(outgoing_ipframe_queue.begin() + i);
    } else {
      ++i;
    }
  }
}

//Compare IP addreses
bool network_helper::compare_ip(octet* ip1, octet* ip2){
  return ip1[0]==ip2[0] && ip1[1]==ip2[1] && ip1[2]==ip2[2] && ip1[3]==ip2[3];
}

//Print IP
void network_helper::print_ip(void* ip)
{
  octet* raw = (octet*)ip;
  printf("%d.%d.%d.%d",
    raw[0],raw[1],raw[2],raw[3]);
}

//Print MAC
void network_helper::print_mac(void* mac)
{
  octet* raw = (octet*)mac;
  printf("%02x:%02x:%02x:%02x:%02x:%02x",
    raw[0],raw[1],raw[2],raw[3],raw[4],raw[5]);
}

//Print bytes in hex for debug
void network_helper::print_bytes(void* buf, int start, int len)
{
  octet * raw = (octet*)buf;
  for(int i=start; i<start+len; i++)
  {
    printf("%02x ", raw[i]);
  }
  std::cout << std::endl;
}

//Print bytes in hex for debug
//Default to start at 0
void network_helper::print_bytes(void* buf, int len)
{
  print_bytes(buf, 0, len);
}
