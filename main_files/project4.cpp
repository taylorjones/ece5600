#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <cstdlib>

int main(int argc, char**argv) {
  char buf[256];
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  sockaddr_in addr, cliaddr;
  std::memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_port = htons(5600);
  if(bind(sockfd, (struct sockaddr *) &addr, sizeof(addr))<0)
    std::exit(1);
  std::cout << "Bind succeeded" << std::endl;
  if(listen(sockfd,1) != 0)
    std::exit(1);
  std::cout << "Listen succeeded" << std::endl;
  socklen_t clilen = sizeof(cliaddr);
  int clisockfd = accept(sockfd, (struct sockaddr *) &cliaddr, &clilen);
  if(clisockfd < 0)
    std::exit(1);
  std::cout << "Connect succeeded" << std::endl;
  std::memset(&buf, 0, 256);
  int len;
  int file = creat("data.txt", 550);
  do {
    len = read(clisockfd, buf, 256);
    write(file, buf, len);
  } while(len != 0);
  close(file);
  close(clisockfd);
  close(sockfd);
  std::cout << "Connection closed" << std::endl;
}
