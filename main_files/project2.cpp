#include "frameio.h"
#include "util.h"
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <cstdlib>
#include <sstream>
#include "frame_structs.h"


frameio net;            // gives us access to the raw network
message_queue ip_queue;  // message queue for the IP protocol stack
message_queue arp_queue; // message queue for the ARP protocol stack
network_helper* nethelper;

//
// This thread sits around and receives frames from the network.
//
void *protocol_loop(void *arg)
{
   ether_frame buf;
   while(1)
   {
      int n = net.recv_frame(&buf,sizeof(buf));
      if ( n < 42 ) continue; // bad frame!
      switch ( buf.prot[0]<<8 | buf.prot[1] )
      {
          case 0x800:
             ip_queue.send(PACKET,&buf,n);
             break;
          case 0x806:
             arp_queue.send(PACKET,&buf,n);
             break;
      }
   }
}

//
// Toy function to print something interesting when an IP frame arrives
//
void *ip_protocol_loop(void *arg)
{
   ether_frame frame;
   event_kind event;
   int timer_no = 1;

   // for fun, fire a timer each time we get a frame
   while ( 1 )
   {
      ip_queue.recv(&event, &frame, sizeof(frame));
      nethelper->processIP(frame.data.as_ip);
      // if ( event != TIMER )
      // {
         //printf("got an IP frame from %d.%d.%d.%d, queued timer %d\n",
         //          buf[12],buf[13],buf[14],buf[15],timer_no);
         //ip_queue.timer(10,timer_no);
         //timer_no++;
      // }
      // else
      // {
         //printf("timer %d fired\n",*(int *)buf);
      // }
   }
}


//
// Toy function to print something interesting when an ARP frame arrives
//
void *arp_protocol_loop(void *arg)
{
   ether_frame frame;
   event_kind event;

   while ( 1 )
   {
      arp_queue.recv(&event, &frame, sizeof(frame));
      nethelper->processArp(frame);
    }
  }

//
// if you're going to have pthreads, you'll need some thread descriptors
//
pthread_t loop_thread, arp_thread, ip_thread;


//
// start all the threads then step back and watch (actually, the timer
// thread will be started later, but that is invisible to us.)
//
int main(int argc, char** argv)
{
   std::string int_name = "eno1";

   octet myip[4] = {129, 123, 4, 194};
   octet gw[4] = {129, 123, 5, 254};
   octet mymac[6] = {0xa0, 0xb3, 0xcc, 0xf7, 0xe4, 0x0c};
   uint32_t subnet = 0xFFFFFE00;
   if(argc > 1) {
     int_name = argv[1];
     std::cout << "IF: " << int_name << std::endl;
   }
   //Allow user to specify their IP
   if(argc > 2) {
    std::istringstream iss(argv[2]);
    std::string token;
    int i=0;
    while (std::getline(iss, token, '.')) {
      if (!token.empty()) {
        myip[i] = std::atoi(token.c_str());
        i++;
      }
      if(i>3)
        break;
    }
    if(i<4) {
      std::cerr << "IP must be four octets."  << std::endl;
      return 1;
    }
    printf("IP: %d.%d.%d.%d\n",
      myip[0],myip[1],myip[2],myip[3]);
   }
   //Allow user to specify their MAC
   if(argc > 3) {
    std::istringstream iss(argv[3]);
    std::string token;
    int i=0;
    while (std::getline(iss, token, ':')) {
      if (!token.empty()) {
        mymac[i] = std::strtol(token.c_str(), NULL, 16);
        i++;
      }
      if(i>5)
        break;
    }
    if(i<6) {
      std::cerr << "MAC must be six octets."  << std::endl;
      return 1;
    }
    printf("MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
      mymac[0],mymac[1],mymac[2],mymac[3],mymac[4],mymac[5]);
   }
   //Allow user to specify their subnet
   if(argc > 4) {
     int netmask = std::atoi(argv[4]);
     if(netmask<0 || netmask>32) {
       std::cerr << "Netmask must be between 0 and 32."  << std::endl;
       return 1;
     }
     subnet = 0xFFFFFFFF & (0xFFFFFFFF << (32 - netmask));
     printf("SN: 0x%08x\n",
       subnet);
   }
   //Allow user to specify their IP
   if(argc > 5) {
    std::istringstream iss(argv[5]);
    std::string token;
    int i=0;
    while (std::getline(iss, token, '.')) {
      if (!token.empty()) {
        gw[i] = std::atoi(token.c_str());
        i++;
      }
      if(i>3)
        break;
    }
    if(i<4) {
      std::cerr << "GW must be four octets."  << std::endl;
      return 1;
    }
    printf("GW: %d.%d.%d.%d\n",
      gw[0],gw[1],gw[2],gw[3]);
   }
   net.open_net(int_name.c_str());
   nethelper = new network_helper(&net, mymac, myip, subnet, gw);

   pthread_create(&loop_thread,NULL,protocol_loop,NULL);
   pthread_create(&arp_thread,NULL,arp_protocol_loop,NULL);
   pthread_create(&ip_thread,NULL,ip_protocol_loop,NULL);
   octet ip[4];
   for ( ; ; ) {
     //Take in an IP to send an ICMP to.
     std::cout << "Please enter an IP to send an IP ping request to (XXX.XXX.XXX.XXX): " << std::endl;
     std::string token;
     std::string line;
     int i=0;
     std::getline(std::cin, line);
     std::istringstream iss(line.c_str());
     while (std::getline(iss, token, '.')) {
       if (!token.empty()) {
         ip[i] = std::atoi(token.c_str());
         i++;
       }
       if(i>3)
         break;
     }
     if(i<4) {
       std::cerr << "IP must be four octets."  << std::endl;
     }
     nethelper->sendICMP(ip);
     sleep(1);
   }
}
