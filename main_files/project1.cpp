
#include "frameio.h"
#include "util.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include "frame_structs.h"

frameio net;             // gives us access to the raw network

//
// This thread sits around and receives frames from the network.
//
void *protocol_loop(void *arg)
{
   ether_frame buf;
   while(1)
   {
      int n = net.recv_frame(&buf,sizeof(ether_frame));
      if ( n < 42 ) continue; // bad frame!
      switch ( buf.prot[0]<<8 | buf.prot[1] )
      {
          case 0x800:{
             ip_frame frame = buf.data.as_ip;
             std::cout << "IP Frame" << std::endl;
             std::cout << "SRC: ";
             print_ip(frame.src_ip);
             std::cout << std::endl;
             std::cout << "DEST: ";
             print_ip(frame.dst_ip);
             std::cout << std::endl;
             }break;
          case 0x806:{
             arp_frame frame = buf.data.as_arp;
             std::cout << "ARP Frame" << std::endl;
             std::cout << "SRC: ";
             print_ip(frame.src_ip);
             std::cout << std::endl;
             std::cout << "DEST: ";
             print_ip(frame.dst_ip);
             std::cout << std::endl;
             }break;
      }
      print_bytes(&buf, 22);
      print_bytes(&buf, 22, 20);
      std::cout << std::endl;
   }
}

pthread_t loop_thread;

int main()
{
   net.open_net("wlp3s0");
   pthread_create(&loop_thread,NULL,protocol_loop,NULL);
   for ( ; ; )
      sleep(1);
}
