#ifndef FRAME_STRUCTS_H
#define FRAME_STRUCTS_H

#include "frameio.h"
#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <map>
#include <cstring>
#include <vector>

struct queued_message
{
  octet ip[4];
  bool reply;
};

//Handy template for arp frames
struct arp_frame
{
  octet hw[2];
  octet prot[2];
  octet hw_size[1];
  octet prot_size[1];
  octet opcode[2];
  octet src_mac[6];
  octet src_ip[4];
  octet dst_mac[6];
  octet dst_ip[4];
  octet data[1472];
};

//Handy template for icmp frames
struct icmp_frame {
  octet type[1];
  octet code[1];
  octet checksum[2];
  octet add_header[4];
  octet data[1470];
};

//Handy template for ip frames
struct ip_frame {
  octet hdr_len[1];
  octet dscp[1];
  octet total_len[2];
  octet ident[2];
  uint16_t frag_ofst:13;
  uint16_t flags:3;
  octet ttl[1];
  octet prot[1];
  octet hdr_chk[2];
  octet src_ip[4];
  octet dst_ip[4];
  union {
    octet raw[1478];
    icmp_frame as_icmp;
  } data;

};
// handy template for 802.3/DIX frames
struct ether_frame
{
   octet dst_mac[6];
   octet src_mac[6];
   octet prot[2];
  union {
    octet raw[1500];
    ip_frame as_ip;
    arp_frame as_arp;
  } data;
};

//Compare ip addresses
struct ip_greater
{
  bool operator () ( const octet * lsh, const octet * rsh )
  {
    return
      lsh[0]>rsh[0]||
        (lsh[0]==rsh[0]&&
          (lsh[1]>rsh[1]||
            (lsh[1]==rsh[1]&&
              (lsh[2]>rsh[2]||
                (lsh[2]==rsh[2]
                  &&lsh[3]>rsh[3]
                    )))));
  };
};

class network_helper {
public:
  std::vector<queued_message> outgoing_message_queue;
  std::vector<ip_frame> outgoing_ipframe_queue;
  std::map<uint32_t, octet *> arp_cache;
  frameio *net;
  octet myMac[6];
  octet myIp[4];
  uint32_t netMask;
  octet gateway[4];
  uint16_t seq = 0;
  uint16_t ident = 0;

  network_helper(frameio *net, octet* mac, octet* ip, uint32_t netmask, octet* gateway) {
    this->net = net;
    std::memcpy(myMac, mac, 6);
    std::memcpy(myIp, ip, 4);
    this->netMask = netmask;
    std::memcpy(this->gateway, gateway, 4);
  }

  uint16_t checksum(octet* frame);
  uint16_t checksum(octet* frame, int len);

  void getRouteIP(octet *ip, octet *result);

  void processIP(ip_frame frame);
  void processICMP(octet* ip, octet* length, icmp_frame frame);
  void processArp(ether_frame frame);

  bool isIPInCache(octet * ip);
  octet * getMACFromCache(octet * ip);
  void putArpCache(octet * ip, octet * mac);

  void sendICMP(octet* ip);
  void sendIPFrame(ip_frame frame);
  void sendARP(octet* ip, bool reply);

  void addArpToQueue(octet* ip, bool reply);
  void checkQueue(octet* ip);

  bool compare_ip(octet* ip1, octet* ip2);

  void print_ip(void* ip);
  void print_mac(void* mac);
  void print_bytes(void* buf, int start, int len);
  void print_bytes(void* buf, int len);

};

#endif //FRAME_STRUCTS_H
